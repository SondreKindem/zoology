﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zoology.Animals;

namespace Zoology
{
    /// <summary>
    /// The program creates different types of animals and 
    /// prints out the sound that each animal makes.
    /// </summary>
    class Program
    {
        private static List<List<Animal>> animalLists = new List<List<Animal>>();
        private static List<Animal> selectedList = new List<Animal>();

        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>() {
                new Lion("Simba"),
                new Retriever("Buddy"),
                new Kiwi("Kenny"),
                new DoggieDogg("Snoop"),
                new Hydra("Henry"),
                new Hippocampus("Horsefish")
            };
            List<Animal> kiwis = new List<Animal>() {
                new Kiwi("Billy"),
                new Kiwi("Benny"),
                new Kiwi("Bartholomew"),
                new Kiwi("Bob"),
            };

            animalLists.Add(animals);
            animalLists.Add(kiwis);

            /*
             * Initial list selection
             */
            SelectNewList();

            /*
             * Main menu
             */
            while (true)
            {
                Console.WriteLine("Current list: " + AnimalListToString(selectedList));

                Console.WriteLine("Commands:\n" +
                    "1: Select new list\n" +
                    "2: Search for animals\n" +
                    "3: Have animals do stuff\n" +
                    "q: Quit");
                Console.Write("> ");

                string input = Console.ReadLine();

                if(input == "1")
                {
                    SelectNewList();
                } 
                else if(input == "2")
                {
                    SelectFilterMethod();
                } 
                else if(input == "3")
                {
                    PrintAnimalInfo(selectedList);
                }
                else if(input == "q")
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Choose between selecting an existing list, or make a new one.
        /// </summary>
        private static void SelectNewList()
        {
            Console.Write("Commands:\n1: add animals manually\n2: use predefined list\n> ");
            string mode = Console.ReadLine();

            if (mode == "1")
            {
                AddAnimalsManual();
            }
            else if (mode == "2")
            {
                SelectPredefinedList();
            }
        }

        /// <summary>
        /// Create a list of animals manually. The new list is set as active.
        /// </summary>
        private static void AddAnimalsManual()
        {
            Console.WriteLine("Add new animals!");

            List<Animal> newAnimalList = new List<Animal>();

            
            // Dynamically get all subtypes of animal! Ripped from https://stackoverflow.com/a/26750
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => typeof(Animal).IsAssignableFrom(p) && !p.IsAbstract)
                .ToList();

            string animalTypesString = "";

            // Print all animal types
            for (int i = 0; i < types.Count(); i++)
            {
                animalTypesString += $"{i}: {types[i].Name}\n";
            }

            // Let the user create as many animals as they want!
            while (true)
            {
                Console.WriteLine("Select an animal type. type \"ok\" to finish");

                Console.WriteLine(animalTypesString);

                try
                {
                    Console.Write("> ");
                    string input = Console.ReadLine();
                    if(input == "ok")  // exit loop if user is done
                    {
                        break;
                    }

                    int selectedType = int.Parse(input);

                    Console.WriteLine("Set the animal's nickname:");
                    Console.Write("> ");
                    string nickname = Console.ReadLine();

                    // Create a new instance of selected animal type. Found at https://stackoverflow.com/a/755
                    var newAnimal = (Animal)Activator.CreateInstance(types[selectedType], nickname);

                    newAnimalList.Add(newAnimal);
                    Console.WriteLine($"Added new {newAnimal.GetType().Name} called {newAnimal.Nickname}");
                }
                catch (FormatException) { }
                catch (ArgumentOutOfRangeException) { }
            }

            if(newAnimalList.Count > 0)
            {
                animalLists.Add(newAnimalList);
                selectedList = newAnimalList;
            }
        }

        /// <summary>
        /// Prints all available lists and lets the user select one of them
        /// </summary>
        private static void SelectPredefinedList()
        {
            Console.WriteLine("Select a list by index");

            // print all available animal lists
            for (int i = 0; i < animalLists.Count; i++)
            {
                Console.WriteLine(i + ": " + String.Join(", ", AnimalListToString(animalLists[i])));
            }

            // Loop in case user enters something invalid
            while (true)
            {
                try
                {
                    Console.Write("> ");
                    int input = int.Parse(Console.ReadLine());

                    selectedList = animalLists[input];

                    // If we reach this the input was valid. Stop retrying
                    break;
                }
                catch (FormatException) { }
                catch (ArgumentOutOfRangeException) { }
            }
        }

        /// <summary>
        /// Let the user choose how to filter the selected list
        /// </summary>
        private static void SelectFilterMethod()
        {
            Console.WriteLine("what do you want to serach by?");
            Console.WriteLine("1: Name\n2: Type\nback (b): go back");

            // Keeps looping until valid input
            while (true)
            {
                Console.Write("> ");
                string input = Console.ReadLine().ToLower();
                if (input == "1")
                {
                    FilterByName();
                    break;
                }
                else if (input == "2")
                {
                    FilterByType();
                    break;
                } 
                else if(input == "back" || input == "b")
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Filter the animals in the currently selected list by name, then set the resulting list as selected.
        /// </summary>
        private static void FilterByName()
        {
            Console.WriteLine("Filtering by name.\nEnter search");
            Console.Write("> ");
            string search = Console.ReadLine().ToLower();

            var filteredAnimals = (from animal in selectedList where animal.Nickname.ToLower().Contains(search) select animal);

            Console.WriteLine("Selected: " + AnimalListToString(filteredAnimals));

            selectedList = filteredAnimals.ToList<Animal>();
        }

        /// <summary>
        /// Filter the animals in the currently selected list by animal type, then set the resulting list as selected.
        /// </summary>
        private static void FilterByType()
        {
            // Get all current unique types using linq method syntax
            var types = selectedList.Select(animal => animal.GetType()).Distinct();

            Console.WriteLine("Available animal types:");

            for (int i = 0; i < types.Count(); i++)
            {
                Console.WriteLine($"{i}: {types.ElementAt(i).Name}");
            }

            // Loop in case user enters something invalid
            while (true)
            {
                try
                {
                    Console.Write("> ");
                    int input = int.Parse(Console.ReadLine());

                    var selectedType = types.ElementAt(input);

                    IEnumerable<Animal> filteredAnimals = selectedList.Where(animal => animal.GetType().Equals(selectedType));

                    selectedList = filteredAnimals.ToList<Animal>();

                    // If we reach this the input was valid. Stop retrying
                    break;
                }
                catch (FormatException) { }
                catch (ArgumentOutOfRangeException) { }
            }
        }

        /// <summary>
        /// Makes the animals in the given list speak and do actions!
        /// </summary>
        private static void PrintAnimalInfo(IEnumerable<Animal> animals)
        {
            Console.WriteLine("Here are some nice animals:");
            // Show off the sound of each animal
            foreach (Animal animal in animals)
            {
                Console.WriteLine();  // Add some spacing
                animal.Speak();

                // Run inherited methods for relevant animals
                if (animal is Kiwi kiwi)
                {
                    // The kiwi is really proud of his pants
                    kiwi.InspectPants();
                }

                if (animal is Mythological mythological)
                {
                    mythological.Hide();
                }

                if (animal is IJump jumperAnimal)
                {
                    jumperAnimal.Jump();
                }

                if (animal is ITacticalSlide sliderAnimal)
                {
                    sliderAnimal.TacticalSlide();
                }
            }
            Console.WriteLine("");
        }


        /// <summary>
        /// Helper method for converting a list of animals into a nice string
        /// </summary>
        /// <returns>A string of the names of all animals in the list</returns>
        private static string AnimalListToString(IEnumerable<Animal> list)
        {
            return String.Join(", ", list.Select(animal => $"{animal.Nickname} ({animal.GetType().Name})"));
        }
    }
}
