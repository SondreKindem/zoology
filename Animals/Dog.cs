﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoology.Animals;

namespace Zoology
{
    public abstract class Dog : Animal, IJump
    {
        public string FurColor { get; set; }

        public Dog(string nickname, string furColor) : base(4, nickname, "woof")
        {
            FurColor = furColor;
        }

        public Dog(int legCount, string nickname, string sound, string furColor) : base(legCount, nickname, sound)
        {
            FurColor = furColor;
        }
          
        public virtual void Fetch()
        {
            Console.WriteLine($"{Nickname} fetches a stick!");
        }

        public abstract void Jump();
    }
}
