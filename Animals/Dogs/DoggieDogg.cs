﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class DoggieDogg : Dog
    {
        public DoggieDogg(string nickname) : base(2, nickname, "sup?", "Gucci") {}

        public override void Fetch()
        {
            Console.WriteLine($"{Nickname} refuses >:(");
        }

        public override void Jump()
        {
            Console.WriteLine($"{Nickname} refuses.");
        }
    }
}
