﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Retriever : Dog
    {
        public Retriever(string nickname) : base(nickname, "Golden") {}

        public override void Jump()
        {
            Console.WriteLine($"{Nickname} jumps a fair distance.");
        }
    }
}
