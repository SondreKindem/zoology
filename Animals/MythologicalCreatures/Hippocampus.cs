﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    /// <summary>
    /// Upper body of a horse, lower of a fish
    /// </summary>
    class Hippocampus : Mythological
    {
        public Hippocampus(string nickname) : base(2, nickname, "the sound of a horse", "greek") {}

        public override void Hide()
        {
            Console.WriteLine($"{Nickname} dives away!");
        }
    }
}
