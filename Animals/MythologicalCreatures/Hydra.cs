﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Hydra : Mythological
    {
        public Hydra(string nickname) : base(0, nickname, "Hiss", "Greek") {}

        public override void Hide()
        {
            Console.WriteLine($"{Nickname} slithers under a rock!");
        }
    }
}
