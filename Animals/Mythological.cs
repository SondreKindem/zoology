﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public abstract class Mythological : Animal
    {
        /// <summary> E.g greek, norse, contemporary </summary>
        public string Origin { get; set; }

        protected Mythological(int legCount, string nickname, string sound, string origin) : base(legCount, nickname, sound)
        {
            Origin = origin;
        }

        /// <summary> Mythological creatures are good at hiding </summary>
        public abstract void Hide();
    }
}
