﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology.Animals
{
    /// <summary>
    /// Lets an animal do a slide
    /// </summary>
    interface ITacticalSlide
    {
        public void TacticalSlide();
    }
}
