﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoology.Animals;

namespace Zoology
{
    public class Lion : Animal, ITacticalSlide
    {
        public string ManeColor {get; set;}

        public Lion(string nickname) : base(4, nickname, "ROAR")
        {
            ManeColor = "brown";
        }

        public Lion(string nickname, string maneColor) : base(4, nickname, "ROAR")
        {
            ManeColor = maneColor;
        }

        public void TacticalSlide()
        {
            Console.WriteLine($"Woah! {Nickname} slides around!");
        }
    }
}
