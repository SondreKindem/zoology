﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public abstract class Animal
    {
        public int LegCount { get; set; }
        public string Nickname { get; set; }
        public string Sound { get; set; }

        public Animal()
        {
            LegCount = 0;
            Nickname = "Unknown";
            Sound = "Hmm";
        }

        public Animal(int legCount, string nickname, string sound)
        {
            LegCount = legCount;
            Nickname = nickname;
            Sound = sound;
        }

        public void Speak()
        {
            Console.WriteLine(Nickname + " goes " + Sound);
        }
    }
}
