﻿using System;
using System.Collections.Generic;
using System.Text;
using Zoology.Animals;

namespace Zoology
{
    public class Kiwi : Animal, ITacticalSlide
    {
        private string[] _pantColors = {"polka dots", "stripes", "cool flames", "fringes"};

        public Kiwi(string nickname) : base(2, nickname, "squark") {}

        public void InspectPants()
        {
            // All kiwis wear pants, and like to show them off
            Console.WriteLine($"{Nickname} is wearing pants with {_pantColors[new Random().Next(_pantColors.Length)]}. Very stylish!");
        }

        public void TacticalSlide()
        {
            Console.WriteLine($"{Nickname} slides 2 metres. Sick!");
        }
    }
}
