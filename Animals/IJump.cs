﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology.Animals
{
    /// <summary>
    /// Lets an animal jump
    /// </summary>
    interface IJump
    {
        public void Jump();
    }
}
